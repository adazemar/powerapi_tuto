# Dépendences

- python >= 3.8
- docker
- git


# récupération des sources et initialisation de l'environement
```
git clone git@github.com:powerapi-ng/hwpc-sensor.git
git clone -b pre-release git@github.com:powerapi-ng/smartwatts-formula.git
git clone git@github.com:powerapi-ng/powerapi.git -b pre-release
python3 -m venv venv
. venv/bin/activate
```

# Création du Dockerfile
```
cd hwpc-sensor
docker build . --tag=hwpc-sensor
cd ..
```


# installation de la formula
```
pip install powerapi
pip install smartwatts-formula
```

# lancement des composants

```
python -m smartwatts --config-file config_formula.json
docker run --net=host --privileged \
	-v /sys:/sys -v /var/lib/docker/containers:/var/lib/docker/containers:ro \
	-v /tmp/powerapi-sensor-reporting:/reporting \
	-v $(pwd):/srv \
	hwpc-sensor --config-file /srv/config_sensor.json

```

